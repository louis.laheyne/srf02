#include <Arduino.h>
#include <Wire.h>
class  SRF02
{
private:
	int reading;
	int sda;
	int scl;
	int addressI2C;
public:
	 SRF02(uint8_t _address, uint8_t _sda, uint8_t _scl);
	~SRF02();
	int LireDistance() ;

};